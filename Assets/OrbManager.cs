﻿using UnityEngine;
using System.Collections;

public class OrbManager : MonoBehaviour {
	
	public Rigidbody orbPrefab;
	public float speed;
	public KeyCode triggerKey;
	public float handOffset;
	private Rigidbody orbClone;
	private bool charging = false;
	private bool handWasVisible = false;
	private bool handVisible = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		GameObject[] hands = GameObject.FindGameObjectsWithTag ("hand");
		GameObject hand = null;

		if (hands.Length != 0) {
			hand = hands [0];
		} else {
			hand = null;
		}

		handVisible = (hand != null);

		//Create
		if (handVisible && !handWasVisible) {
			orbClone = (Rigidbody) Instantiate (orbPrefab, hand.transform.position, hand.transform.rotation);
			charging = true;
		}
		//Charge
		if (charging && orbClone != null && handVisible && handWasVisible) {
			orbClone.transform.position = hand.transform.position + r.forward * handOffset;
		}
		//Fire
		if (orbClone != null && handWasVisible && !handVisible) {
			orbClone.velocity = transform.forward * speed;
			charging = false;
		}

		handWasVisible = (hand != null);
		/*
		if (Input.GetKeyDown (triggerKey)) {
			orbClone = (Rigidbody) Instantiate (orbPrefab, transform.position + transform.forward * handOffset, transform.rotation);
			charging = true;
		}
		if (orbClone != null) {
			if (Input.GetKeyUp (triggerKey)) {
				orbClone.velocity = transform.forward * speed;
				charging = false;
			}
		}
		if (charging && orbClone != null) {
			orbClone.transform.position = transform.position + transform.forward * handOffset;
		}*/
	}
}
