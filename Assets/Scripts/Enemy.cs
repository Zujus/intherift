﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour 
{
	Transform player;               // Reference to the player's position.
	NavMeshAgent nav;               // Reference to the nav mesh agent.
	Animator anim;                              // Reference to the animator.
	AudioSource enemyAudio;                     // Reference to the audio source.
	ParticleSystem hitParticles;                // Reference to the particle system that plays when the enemy is damaged.
//	CapsuleCollider capsuleCollider;            // Reference to the capsule collider.
	bool isDead = false;

	// Use this for initialization
	void Awake ()
	{
		// Setting up the references.
		anim = GetComponent <Animator> ();
		enemyAudio = GetComponentInChildren <AudioSource> ();
		hitParticles = GetComponentInChildren <ParticleSystem> ();
//		capsuleCollider = GetComponent <CapsuleCollider> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		nav = GetComponent <NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!isDead)
		{
			nav.SetDestination (player.position);
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "projectile") {
			Die ();
			Destroy(other.gameObject);
		}
	}

	public void Die ()
	{
		isDead = true;
		enemyAudio.Play();
		hitParticles.Play();
		nav.enabled = false;

		anim.SetBool ("Dead", true);

		Debug.Log ("Enemy killed");
		Destroy (gameObject, 1f);
	}
}
