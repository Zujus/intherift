﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {

	public GameObject enemy;                // The enemy prefab to be spawned.
	public float spawnTimeout = 2f;
	private float spawnTimer;
	ParticleSystem mistParticles;                // Reference to the particle system that plays when the enemy is damaged.
	public int maxSpawns = 3;
	private int spawned = 0;

	// Use this for initialization
	void Start ()
	{
		spawnTimer = spawnTimeout;
		mistParticles = GetComponentInChildren <ParticleSystem> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		spawnTimer -= Time.deltaTime;
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			Spawn ();
		}
		
	}

	void OnTriggerStay(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			Spawn ();
		}
		
	}

	void Spawn()
	{
		if (spawnTimer <= 0 && spawned < maxSpawns) 
		{
			Instantiate (enemy, transform.position, transform.rotation);
			spawnTimer = spawnTimeout;
			spawned++;
		}
	}

}
