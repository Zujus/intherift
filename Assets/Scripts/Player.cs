﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	public float attackRange = 5;
	//private Light light;
	
	public float lightRangeLow = 5f;
	public float lightRangeHigh = 15f;
	public float lightStep = 5f;
	public float lightRetractTimeout = 2f;

	private float lightRetractTimer = 0f;
	
	private bool expandLight = false;
	private bool retractLight = false;

	private Light light;

	public Texture2D screenTexture;

	private bool isDead = false;
	private float resetTimeout = 3.5f;

	AudioSource dieAudio; 

	// Use this for initialization
	void Start ()
	{
		Debug.Log("Start");
		light = GetComponentInChildren<Light> ();
		dieAudio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (AttackTrigger()) 
		{
			Attack ();
			ExpandLight();
		}
		AdjustLightRange ();

		if (isDead) {
			resetTimeout -= Time.deltaTime;
			if (resetTimeout <= 0) {
				Application.LoadLevel(Application.loadedLevel);
			}
		}
	}

	bool AttackTrigger()
	{
		return Input.GetKeyUp (KeyCode.R);
	}

	void Attack()
	{
		Debug.Log("Attacking");

		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");

		foreach (var enemy in enemies) 
		{
			if (IsVisible(enemy) && IsInRange(enemy))
			{
				Debug.Log ("Enemy in range");

				KillEnemy(enemy);
			} else 
			{
			}
		}
	}

	bool IsInRange(GameObject o)
	{
		var distance = Vector3.Distance (transform.position, o.transform.position);
		Debug.Log (distance);
		return distance <= attackRange;
	}


	bool IsVisible(GameObject o)
	{
		return o.renderer.isVisible;
	}

	void KillEnemy(GameObject o) 
	{
		Enemy enemy = o.GetComponent<Enemy> ();
		enemy.Die ();
	}

	void ExpandLight()
	{
		expandLight = true;
		retractLight = false;
	}

	void AdjustLightRange()
	{
		if (expandLight)
		{
			if (light.range >= lightRangeHigh)
			{
				light.range = lightRangeHigh;
				expandLight = false;
				retractLight = true;
				lightRetractTimer = 0f;
			}
			else light.range += lightStep * Time.deltaTime;

		} else if (retractLight) 
		{
			lightRetractTimer += Time.deltaTime;

			if (light.range <= lightRangeLow)
			{
				light.range = lightRangeLow;
				expandLight = false;
				retractLight = false;
			}
			else if (lightRetractTimer >= lightRetractTimeout)
			{
				light.range -= lightStep * Time.deltaTime;
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			Die ();
		}
		
	}

	void Die()
	{
		var controller = GetComponent<OVRPlayerController> ();
		controller.Acceleration = 0;
		dieAudio.Play();

		isDead = true;

	}
}
