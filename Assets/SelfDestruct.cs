﻿using UnityEngine;
using System.Collections;

public class SelfDestruct : MonoBehaviour {
	public float timeToLive;
	public GameObject parent;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log (timeToLive);
		timeToLive = timeToLive - Time.deltaTime;
		if (timeToLive < 0) {
			Destroy (parent);
		}
	}
}
